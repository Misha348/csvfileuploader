﻿using MainProject.DB.Repository.Interfaces;
using MainProject.Entities.BaseEntity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MainProject.DB.Repository.Implementation
{
	public class BaseRepo<TEntity, TId> : IBaseRepo<TEntity, TId> where TEntity : class, IBaseEntity<TId>
	{
		public DbContext Context { get; }
		public DbSet<TEntity> DbSet { get; }

		public BaseRepo(DbContext context)
		{
			Context = context;
			DbSet = Context.Set<TEntity>();
		}

		public virtual IQueryable<TEntity> GetAll()
		{
			var query = DbSet.AsQueryable();			
			return DbSet.AsQueryable();
		}

		public virtual TEntity GetSingle(object id)
		{
			var entity = DbSet.Find(id);
			return entity;
		}

		public virtual async Task<TId> Add(TEntity entity)
		{
			await DbSet.AddAsync(entity);
			Context.SaveChanges();
			return entity.Id;
		}

		public virtual void AddRange(IEnumerable<TEntity> entities)
		{			
			DbSet.AddRange(entities);
			Context.SaveChanges();
		}

		public virtual void Remove(TEntity entity)
		{
			DbSet.Remove(entity);
			Context.SaveChanges();
		}

		public virtual void Remove(object id)
		{
			var entity = DbSet.Find(id);
			DbSet.Remove(entity);
			Context.SaveChanges();
		}

		
	}
}
