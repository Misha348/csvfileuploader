﻿using MainProject.DB.Repository.Interfaces;
using MainProject.Entities;

namespace MainProject.DB.Repository.Implementation
{
	public class FileUploadRepo : BaseRepo<ImportedFileRowData, int>, IFileUploadRepo 
	{
		public FileUploadRepo(AppDbContext context) : base(context) { }
	}
}
