﻿using MainProject.Entities.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MainProject.DB.Repository.Interfaces
{
	public interface IBaseRepo<TEntity, TId> where TEntity : class, IBaseEntity<TId>
	{	
		IQueryable<TEntity> GetAll();
		TEntity GetSingle(object id);
		Task<TId> Add(TEntity entity);
		void AddRange(IEnumerable<TEntity> entities);
		void Remove(TEntity entity);
		void Remove(object id);

	}
}
