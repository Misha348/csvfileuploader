﻿using MainProject.Entities;

namespace MainProject.DB.Repository.Interfaces
{
	public interface IFileUploadRepo:IBaseRepo<ImportedFileRowData, int>
	{
	}
}
