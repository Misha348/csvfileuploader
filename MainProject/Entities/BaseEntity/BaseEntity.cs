﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MainProject.Entities.BaseEntity
{
	public abstract class BaseEntity<T> : IBaseEntity<T>
	{
		[Key]
		public virtual T Id { get; set; }
		public virtual DateTime? DateOfBirth { get; set; }
	}
}
