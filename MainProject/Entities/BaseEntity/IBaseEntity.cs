﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MainProject.Entities.BaseEntity
{
	public interface IBaseEntity<T>
	{
		T Id { get; set; }
		DateTime? DateOfBirth { get; set; }
	}
}
