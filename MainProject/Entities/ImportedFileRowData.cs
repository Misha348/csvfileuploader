﻿using MainProject.Entities.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MainProject.Entities
{
	[Table("tblFileData")]
	public class ImportedFileRowData: BaseEntity<int>
	{		
		public string Name { get; set; }		
		public bool IsMarried { get; set; }
		public string Phone { get; set; }

		[Column(TypeName = "decimal(18,4)")]
		public decimal Salary { get; set; }
	}
}
