﻿using MainProject.Models;
using MainProject.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace MainProject.Controllers
{
	public class FileController : Controller
	{
		private readonly IWebHostEnvironment _env;
		private readonly IFileUploadDataService _fileService;
		public FileController(IWebHostEnvironment env, IFileUploadDataService fileService)
		{
			_env = env;
			_fileService = fileService;
		}

		[HttpGet]
		public async Task<IActionResult> Index()
		{
			var userModel = _fileService.GetAllUsers();
			return await Task.Run(() => View(userModel));
		}

		[HttpPost]
		public async Task<IActionResult> FileDownloader(IFormFile file)
		{
			var userModel = _fileService.GetAllUsers();
			ViewBag.Message = null;
			ViewBag.ConvertationIsSucces = null;

			if (file == null)
			{
				ViewBag.Message = "invalid file format";
				return await Task.Run(() => View("Index", userModel));
			}				
			
			bool convertationIsSucces = false;
			if (Path.GetExtension(file.FileName) == ".csv" || Path.GetExtension(file.FileName) == ".xlsx")
			{
				convertationIsSucces = _fileService.HandleUploadedFile(file);
				userModel = _fileService.GetAllUsers();
				ViewBag.ConvertationIsSucces = convertationIsSucces;
				return await Task.Run(() => View("Index", userModel));
			}

			ViewBag.Message = "invalid file format";
			return await Task.Run(() => View("Index", userModel));			
		}

		[HttpGet]		
		public async Task<IActionResult> DeleteRequest(int id)
		{
			if (id == 0)
			{
				Response.StatusCode = 403;
				return PartialView("Error");				
			}
			return await Task.Run(() => View(_fileService.GetSingleUser(id)));
		}

		[HttpPost]
		public async Task<JsonResult> DeleteConfirmation(int id)
		{
			_fileService.RemoveUser(id);
			return await Task.Run(() => Json(new { status = "Success" }));
		}
	}
}
