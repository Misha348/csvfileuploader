﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MainProject.Models
{
	public class UserVM
	{
		[Display(Name = "Id")]
		public int Id { get; set; }

		[Display(Name = "User name")]
		public string Name { get; set; }

		[Display(Name = "Date of birth")]

		public DateTime? DateofBirth { get; set; }

		[Display(Name = "Is married")]
		public bool IsMarried { get; set; }

		[Display(Name = "Phone")]
		public string Phone { get; set; }

		[Display(Name = "Salary")]
		public decimal Salary { get; set; }
	}
}
