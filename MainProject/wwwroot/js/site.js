﻿
// Nice table sorting
//$(document).ready(function () {
//	$('#records').DataTable();
//});


// Another table sorting
//new Tablesort(document.getElementById('records'), {
//	descending: true
//});


//Ordinar table sorting
window.onload = function () { makeAllSortable(); };
function sortTable(table, col, reverse) {
	var tb = table.tBodies[0],
		tr = Array.prototype.slice.call(tb.rows, 0),
		i;
	reverse = -((+reverse) || -1);
	tr = tr.sort(function (a, b) {
		return reverse * (a.cells[col].textContent.trim()
			.localeCompare(b.cells[col].textContent.trim()));
	});
	for (i = 0; i < tr.length; ++i) tb.appendChild(tr[i]);
}

function makeSortable(table) {
	var th = table.tHead, i;
	th && (th = th.rows[0]) && (th = th.cells);
	if (th) i = th.length;
	else return;
	while (--i >= 0) (function (i) {
		var dir = 1;
		th[i].addEventListener('click', function () { sortTable(table, i, (dir = 1 - dir)) });
	}(i));
}

function makeAllSortable(parent) {
	parent = parent || document.body;
	var t = parent.getElementsByTagName('table'), i = t.length;
	while (--i >= 0) makeSortable(t[i]);
}



// table searching
(function (document) {
	'use strict';

	var TableFilter = (function (myArray) {
		var search_input;

		function _onInputSearch(e) {
			search_input = e.target;
			var tables = document.getElementsByClassName(search_input.getAttribute('data-table'));
			myArray.forEach.call(tables, function (table) {
				myArray.forEach.call(table.tBodies, function (tbody) {
					myArray.forEach.call(tbody.rows, function (row) {
						var text_content = row.textContent.toLowerCase();
						var search_val = search_input.value.toLowerCase();
						row.style.display = text_content.indexOf(search_val) > -1 ? '' : 'none';
					});
				});
			});
		}

		return {
			init: function () {
				var inputs = document.getElementsByClassName('search-input');
				myArray.forEach.call(inputs, function (input) {
					input.oninput = _onInputSearch;
				});
			}
		};
	})(Array.prototype);

	document.addEventListener('readystatechange', function () {
		if (document.readyState === 'complete') {
			TableFilter.init();
		}
	});

})(document);



