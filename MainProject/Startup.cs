using MainProject.DB;
using MainProject.DB.Repository.Implementation;
using MainProject.DB.Repository.Interfaces;
using MainProject.Services.Implementation;
using MainProject.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MainProject
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddDbContext<AppDbContext>(opt =>
				opt.UseSqlServer(Configuration
					.GetConnectionString("DefaultConnection")));

			services.AddScoped<IFileUploadRepo, FileUploadRepo>();
			services.AddScoped<IFileUploadDataService, FileUploadDataService>();
			services.AddControllersWithViews();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
			}
			app.UseStaticFiles();

			if (!Directory.Exists(Path.Combine(env.ContentRootPath, "Uploads")))
			{
				Directory.CreateDirectory(Path.Combine(env.ContentRootPath, "Uploads"));
			}
			app.UseStaticFiles(new StaticFileOptions
			{

				FileProvider = new PhysicalFileProvider(

					Path.Combine(env.ContentRootPath, "Uploads")),
				RequestPath = "/Files"
			});

			app.UseRouting();

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller=File}/{action=Index}/{id?}");
			});
		}
	}
}
