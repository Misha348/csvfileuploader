﻿using MainProject.Models;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace MainProject.Services.Interfaces
{
	public interface IFileUploadDataService
	{
		bool HandleUploadedFile(IFormFile file);
		IEnumerable<UserVM> GetAllUsers();
		UserVM GetSingleUser(int id);
		void AddUsers(IEnumerable<UserVM> users);
		void RemoveUser(int id);
	}
}
