﻿using MainProject.DB.Repository.Interfaces;
using MainProject.Entities;
using MainProject.Helpers;
using MainProject.Helpers.Interfaces;
using MainProject.Models;
using MainProject.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MainProject.Services.Implementation
{
	class FileUploadDataService : IFileUploadDataService, IFileHandler
	{
		private readonly IFileUploadRepo _fileUploadRepo;
		private readonly Dictionary<string, FileDataTransfer> _fileDataOperationStrategy; 
		private FileServerSaver _fileServerSaver { get;  set; }		

		public FileUploadDataService(IFileUploadRepo fileUploadRepo)
		{
			_fileUploadRepo = fileUploadRepo;	
			_fileDataOperationStrategy = new Dictionary<string, FileDataTransfer>();

			SetAcceptableExtensions();
		}

		private void SetAcceptableExtensions()
		{
			_fileDataOperationStrategy.Add(".csv", new Csv_FileDataManager());
			_fileDataOperationStrategy.Add(".xlsx", new Xlsx_FileDataManager());
		}

		public bool HandleUploadedFile(IFormFile file)
		{			
			SaveFileToServer(file);			
			return FileDataToDBAsObjectListIsTransfered(file);
		}

		public void SaveFileToServer(IFormFile file)
		{
			_fileServerSaver = new FileServerSaver(file);
			_fileServerSaver.SaveFileToServer();		
		}

		/// <summary>
		/// Convert achieved file to collection of UserVM
		/// </summary>
		/// <param name="file">file uploaded by user</param>
		/// <returns>boolen value that indicades whether succesfull file convertation was provided</returns>
		public bool FileDataToDBAsObjectListIsTransfered(IFormFile file)
		{
			var ext = Path.GetExtension(file.FileName);		

			var usersModel = _fileDataOperationStrategy[ext].GetFileDataAsUserVMCollection(_fileServerSaver.SavedFilePath);
			return CheckIfFileDataConvertationIsValid(usersModel);

			#region if/else replaced by  Dictionary<string, FileDataTransfer> usage
			//if (Path.GetExtension(file.FileName) == ".csv")
			//{
			//	_fileDataManager = new Csv_FileDataManager();
			//	usersModel = _fileDataManager.GetFileDataAsUserVMCollection(_fileServerSaver.SavedFilePath);
			//	return CheckIfFileDataConvertationIsValid(usersModel);
			//}
			//else if (Path.GetExtension(file.FileName) == ".xlsx")
			//{
			//	_fileDataManager = new Xlsx_FileDataManager();
			//	usersModel = _fileDataManager.GetFileDataAsUserVMCollection(_fileServerSaver.SavedFilePath);
			//	return CheckIfFileDataConvertationIsValid(usersModel);
			//}
			//return false;
			#endregion
		}

		private bool CheckIfFileDataConvertationIsValid(List<UserVM> usersModel)
		{
			if (usersModel != null)
			{
				AddUsers(usersModel);
				return true;
			}
			return false;
		}

		public UserVM GetSingleUser(int id)
		{
			var query = _fileUploadRepo.GetSingle(id);
			return  new UserVM 
			{  
				Id = query.Id,
				Name = query.Name,
				DateofBirth = query.DateOfBirth,
				IsMarried = query.IsMarried,
				Phone = query.Phone,
				Salary = query.Salary
			};
		}

		public IEnumerable<UserVM> GetAllUsers()
		{
			var query = _fileUploadRepo.GetAll();
			List<UserVM> users = query.Select(qItem => new UserVM()
			{
				Id = qItem.Id,
				Name = qItem.Name,
				IsMarried = qItem.IsMarried,
				Phone = qItem.Phone,
				Salary = qItem.Salary,
				DateofBirth = qItem.DateOfBirth
			}).ToList();

			return users;
		}

		public void AddUsers(IEnumerable<UserVM> users)
		{
			List<ImportedFileRowData> rowsData = users.Select(user => new ImportedFileRowData()
			{
				Name = user.Name,
				IsMarried = user.IsMarried, 
				Phone = user.Phone,
				Salary = user.Salary,
				DateOfBirth = user.DateofBirth
			}).ToList();
			_fileUploadRepo.AddRange(rowsData);
		}	

		public void RemoveUser(int id)
		{
			_fileUploadRepo.Remove(id);
		}

		
	}
}
