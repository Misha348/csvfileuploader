﻿using MainProject.Models;
using System;
using System.Collections.Generic;
using ExcelDataReader;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;

namespace MainProject.Helpers
{
	public interface FileDataTransfer
	{
		List<UserVM> GetFileDataAsUserVMCollection(string savedFilePath);
	}
	public class FileDataCheker
	{		
		/// <summary>
		/// Method-helper that conver different date format types
		/// </summary>
		/// <param name="fileRowValue"> string that represent one row date value from current file</param>
		/// <returns></returns>
		public DateTime ConvertDateValue(string dateRowValue)
		{
			char[] dateSeparators = { '/', '-', '.', '@', '#', ':' };
			string[] validformats = { "MM?dd?yyyy", "yyyy?MM?dd", "dd?MM?yyyy",
										"MM?dd?yyyy H:mm:ss", "yyyy?MM?dd H:mm:ss", "dd?MM?yyyy H:mm:ss" };

			List<string> f = new List<string>();
			foreach (char c in dateSeparators)
			{
				for (int i = 0; i < validformats.Length; i++)
					f.Add(validformats[i].Replace('?', c));
			}
			string[] formats = f.ToArray();

			DateTime date;
			if (DateTime.TryParseExact(dateRowValue, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
			{				
				return date;
			}				
			else
				return default;
		}

		/// <summary>
		/// Method-helper that conver string that represent boolean value
		/// </summary>
		/// <param name="isMarriedStatusRowValue">string that represent one row isMarried value from current file</param>
		/// <returns></returns>
		public bool ConvertIsMarriedStatus(string isMarriedStatusRowValue)
		{
			bool isMarried;

			if (bool.TryParse(char.ToUpper(isMarriedStatusRowValue[0]) + isMarriedStatusRowValue.Substring(1), out isMarried))
				return isMarried;
			else
				return default;
		}

		public decimal ConvertSalaryValue(string salaryValue)
		{
			decimal salary;
			var style = NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands;
			if (decimal.TryParse(salaryValue, style, CultureInfo.InvariantCulture, out salary))
				return salary;
			else
				return 0;		
		}	
	}

	public class Csv_FileDataManager : FileDataCheker, FileDataTransfer
	{
		
		/// <summary>
		/// Convert achived file data to UserVM collection from .CSV file
		/// </summary>
		/// <param name="savedFilePath">full path on server where current file was saved</param>
		/// <returns></returns>
		public List<UserVM> GetFileDataAsUserVMCollection(string savedFilePath)
		{
			List<UserVM> usersModel = new List<UserVM>();

			// Holds file row data values
			List<string[]> fileRows = new List<string[]>();

			using (StreamReader reader = new StreamReader(savedFilePath))
			{
				try
				{
					char[] separator = { ',', ' ', ';' };
					while (!reader.EndOfStream)
					{
						fileRows.Add(reader.ReadLine().Split(separator));
					}
				}
				catch (Exception)
				{
					return null;
				}

				try
				{
					usersModel = fileRows.Select(row => new UserVM
					{
						Name = row[0],
						DateofBirth = ConvertDateValue(row[1]),
						IsMarried = ConvertIsMarriedStatus(row[2]),
						Phone = row[3],
						Salary = ConvertSalaryValue(row[4]) //decimal.Parse(row[4], CultureInfo.InvariantCulture)
					}).ToList();
				}
				catch (Exception)
				{
					return null;
				}				
			}
			return usersModel;
		}
	}

	/// <summary>
	/// Convert achived file data to UserVM collection from .XLXS file
	/// </summary>
	public class Xlsx_FileDataManager : FileDataCheker, FileDataTransfer
	{		
		public List<UserVM> GetFileDataAsUserVMCollection(string savedFilePath)
		{			
			List<UserVM> usersModel = new List<UserVM>();

			System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
			using (var fileStream = File.Open(savedFilePath, FileMode.Open, FileAccess.Read))
			{
				using (var reader = ExcelReaderFactory.CreateReader(fileStream))
				{
					try
					{
						while (reader.Read())
						{							
							var user = new UserVM();

							usersModel.Add(new UserVM
							{
								Name = reader.GetValue(0).ToString(),
								DateofBirth = ConvertDateValue(reader.GetValue(1).ToString()),
								IsMarried = ConvertIsMarriedStatus(reader.GetValue(2).ToString()),
								Phone = reader.GetValue(3).ToString(),
								Salary = ConvertSalaryValue(reader.GetValue(4).ToString()) //decimal.Parse(reader.GetValue(4).ToString(), CultureInfo.InvariantCulture)
							});
						}
					}
					catch (Exception)
					{
						return null;
					}
				}
			}
			return usersModel;
		}
	}
}
