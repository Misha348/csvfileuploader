﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MainProject.Helpers
{
	public class FileServerSaver
	{
		private readonly IFormFile _file;	
		public string SavedFilePath { get; private set; }
		public FileServerSaver(IFormFile file)
		{
			_file = file;			
		}

		public void SaveFileToServer()
		{
			string ext = Path.GetExtension(_file.FileName);
			var serverPath = Directory.GetCurrentDirectory();
			var folderName = "Uploads";
			var uploadFolderPath = Path.Combine(serverPath, folderName);

			string fileName = Path.GetFileNameWithoutExtension(Path.GetRandomFileName()) + ext;
			SavedFilePath = Path.Combine(uploadFolderPath, fileName);

			using (var fileStream = new FileStream(SavedFilePath, FileMode.Create))
			{
				_file.CopyTo(fileStream);
				fileStream.Flush();
			}
		}
	}
}
