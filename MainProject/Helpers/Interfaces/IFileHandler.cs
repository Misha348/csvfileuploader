﻿using Microsoft.AspNetCore.Http;

namespace MainProject.Helpers.Interfaces
{
	public interface IFileHandler
	{
		void SaveFileToServer(IFormFile file);
		bool FileDataToDBAsObjectListIsTransfered(IFormFile file);
	}
}
