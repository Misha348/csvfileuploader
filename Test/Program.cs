﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Test
{
	public interface ISomeOperation
	{
		//public void DoOperation(string data);
	}
	public abstract class BaseClass : ISomeOperation
	{
		public void DoOperation(string data)
		{
			Console.WriteLine("result form BASE");
		}
	}
	public class OperationA : ISomeOperation
	{
		
	}

	public class OperationB : ISomeOperation
	{
		
	}

	public class OperationC : ISomeOperation
	{

	}

	public class Executor
	{
		private readonly Dictionary<string, ISomeOperation> _operationStrategy = new Dictionary<string, ISomeOperation>();
		public Executor()
		{
			_operationStrategy.Add("1", new OperationA());
			_operationStrategy.Add("2", new OperationB());
			_operationStrategy.Add("3", new OperationC());
		}
	}

	class Program
	{
		static void Main(string[] args)
		{



			//DateTime dt = new DateTime(2042, 12, 24, 18, 42, 0);

			//Console.WriteLine(dt.ToString("MM'/'dd yyyy"));
			//Console.WriteLine(dt.ToString("dd.MM.yyyy"));
			//Console.WriteLine(dt.ToString("MM.dd.yyyy HH:mm"));
			//Console.WriteLine(dt.ToString("dddd, MMMM (yyyy): HH:mm:ss"));
			//Console.WriteLine(dt.ToString("dddd @ hh:mm tt", System.Globalization.CultureInfo.InvariantCulture));
			//===========================================================//

			//string date = "12.28.2019";
			//string[] validformats = { "MM?dd?yyyy", "yyyy?MM?dd", "dd?MM?yyyy",
			//							"MM?dd?yyyy HH:mm", "yyyy?MM?dd HH:mm", "dd?MM?yyyy HH:mm" };

			//CultureInfo provider = new CultureInfo("en-US");
			//try
			//{
			//	DateTime dateTime = DateTime.ParseExact(date, validformats, CultureInfo.InvariantCulture, );
			//	Console.WriteLine("The specified date is valid: " + dateTime);
			//}
			//catch (FormatException)
			//{
			//	Console.WriteLine("Unable to parse the specified date");
			//}
			//===========================================================//

			//char[] dateSeparators = { '/', '-', '.', '@', '#', ':' };
			//string[] validformats = { "MM?dd?yyyy", "yyyy?MM?dd", "dd?MM?yyyy",
			//							"MM?dd?yyyy H:mm:ss", "yyyy?MM?dd H:mm:ss", "dd?MM?yyyy H:mm:ss" };
			
			//List<string> f = new List<string>();
			//foreach (char c in dateSeparators)
			//{
			//	for (int i = 0; i < validformats.Length; i++)				
			//		f.Add(validformats[i].Replace('?', c));							
			//}
			//string[] formats = f.ToArray();
			//DateTime dt;
			//DateTime.TryParseExact("29.01.2010 0:00:00", formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
			
			//Console.WriteLine(dt.ToShortDateString());
		}
	}
	
}
